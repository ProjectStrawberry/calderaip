package com.gmail.jd4656.iplogger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CommandIPLookup implements CommandExecutor {
    private IPLogger plugin;
    CommandIPLookup(IPLogger p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("iplogger.admin")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        if (args.length < 1) {
            sender.sendMessage(ChatColor.GOLD + "Usage: /iplookup <player> - Returns a list of IP's used by this player.");
            return true;
        }

        Player player = Bukkit.getPlayer(args[0]);
        String uuid;

        if (player != null) {
            uuid = player.getUniqueId().toString();
        } else {
            try {
                PreparedStatement pstmt;
                ResultSet rs;
                Connection c = plugin.getConnection();

                pstmt = c.prepareStatement("SELECT count(*) FROM users WHERE lower(name)=?");
                pstmt.setString(1, args[0].toLowerCase());

                rs = pstmt.executeQuery();
                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                    pstmt.close();
                    return true;
                } else {
                    pstmt = c.prepareStatement("SELECT * FROM users WHERE lower(name)=?");
                    pstmt.setString(1, args[0].toLowerCase());

                    rs = pstmt.executeQuery();
                    uuid = rs.getString("uuid");
                }
            } catch (Exception e) {
                plugin.getLogger().severe("/iplookup: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        try {
            PreparedStatement pstmt;
            ResultSet rs;
            Connection c = plugin.getConnection();

            pstmt = c.prepareStatement("SELECT * FROM users WHERE uuid=? ORDER BY lastSeen DESC");
            pstmt.setString(1, uuid);
            rs = pstmt.executeQuery();

            String ips = "";
            boolean first = true;
            while (rs.next()) {
                if (!first) {
                    ips += ", ";
                } else {
                    first = false;
                }
               ips += rs.getString("ip");
            }

            sender.sendMessage(ChatColor.DARK_GREEN + "IPs used by " + ChatColor.GOLD + args[0] + ":");
            sender.sendMessage(ChatColor.GOLD + ips);
            pstmt.close();
            return true;
        } catch (Exception e) {
            plugin.getLogger().severe("/iplookup (2): " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
            return true;
        }
    }
}
