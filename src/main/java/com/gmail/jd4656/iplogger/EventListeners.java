package com.gmail.jd4656.iplogger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class EventListeners implements Listener {
    private IPLogger plugin;

    EventListeners(IPLogger p) {
        plugin = p;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String ip = player.getAddress().getAddress().getHostAddress();

        if (plugin.whitelist.contains(player.getName().toLowerCase())) return;

        try {
            PreparedStatement pstmt;
            ResultSet rs;
            Connection c = plugin.getConnection();

            pstmt = c.prepareStatement("SELECT count(*) FROM users WHERE uuid=?");
            pstmt.setString(1, player.getUniqueId().toString());
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                pstmt = c.prepareStatement("SELECT count(*) FROM users WHERE ip=? AND uuid !=?");
                pstmt.setString(1, ip);
                pstmt.setString(2, player.getUniqueId().toString());
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") > 0) {
                    String msg = "";
                    pstmt = c.prepareStatement("SELECT * FROM users WHERE ip=? AND uuid !=?");
                    pstmt.setString(1, ip);
                    pstmt.setString(2, player.getUniqueId().toString());
                    rs = pstmt.executeQuery();

                    while (rs.next()) {
                        if (!msg.equals("")) msg += ", ";
                        msg += rs.getString("name") + " (" + rs.getString("ip") + ")";
                    }

                    Bukkit.broadcast(ChatColor.DARK_RED + "[IPLogger] " + ChatColor.RESET + player.getDisplayName() + ChatColor.GOLD + " has shared an IP with the following users: " + msg, "iplogger.notify");
                }
            }

            pstmt = c.prepareStatement("SELECT count(*) FROM users WHERE uuid=? AND ip=?");
            pstmt.setString(1, player.getUniqueId().toString());
            pstmt.setString(2, ip);
            rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                pstmt = c.prepareStatement("INSERT INTO users (uuid, name, ip, lastSeen) VALUES (?, ?, ?, ?)");
                pstmt.setString(1, player.getUniqueId().toString());
                pstmt.setString(2, player.getName());
                pstmt.setString(3, ip);
                pstmt.setLong(4, System.currentTimeMillis());
                pstmt.executeUpdate();
            } else {
                pstmt = c.prepareStatement("UPDATE users SET name=?, lastSeen=? WHERE uuid=? AND ip=?");
                pstmt.setString(1, player.getName());
                pstmt.setLong(2, System.currentTimeMillis());
                pstmt.setString(3, player.getUniqueId().toString());
                pstmt.setString(4, ip);
                pstmt.executeUpdate();
            }

            pstmt.close();

        } catch (Exception e) {
            plugin.getLogger().severe("onPlayerJoin " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }
}
