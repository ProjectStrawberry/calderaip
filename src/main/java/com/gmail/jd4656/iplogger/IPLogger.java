package com.gmail.jd4656.iplogger;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class IPLogger extends JavaPlugin {
    IPLogger plugin;
    File dbFile;
    List<String> whitelist = new ArrayList<>();
    private Connection conn = null;

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        dbFile = new File(plugin.getDataFolder(), "IPLogger.db");
        if (!dbFile.exists()) {
            try {
                dbFile.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().severe("File write error: IPLogger.db");
                return;
            }
        }

        try {
            Statement stmt;
            Connection c = getConnection();

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS users (uuid TEXT, name TEXT, ip TEXT, lastSeen INT)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS whitelist (name TEXT)");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM whitelist");

            while (rs.next()) whitelist.add(rs.getString("name"));

            stmt.close();
        } catch (Exception e) {
            plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            plugin.getLogger().warning("IPLogger failed to load.");
            return;
        }

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);

        this.getCommand("iplookup").setExecutor(new CommandIPLookup(this));
        this.getCommand("alts").setExecutor(new CommandAlts(this));
        this.getCommand("iplogger").setExecutor(new CommandIPLogger(this));

        plugin.getLogger().info("IPLogger loaded.");
    }

    @Override
    public void onDisable() {
        if (conn != null) {
            try {
                conn.close();
            } catch (Exception ignored) {}
        }
    }

    Connection getConnection() throws Exception {
        if (conn == null ) {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
        }
        return conn;
    }
}
