package com.gmail.jd4656.iplogger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.util.StringUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CommandIPLogger implements TabExecutor {
    private IPLogger plugin;

    CommandIPLogger(IPLogger p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("iplogger.admin")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        if (args.length < 1) {
            args = new String[]{"help"};
        }

        if (args[0].equals("?") || args[0].equals("help")) {
            sender.sendMessage(ChatColor.DARK_GREEN + "IPLogger Commands:");
            sender.sendMessage(ChatColor.DARK_GREEN + "/iplookup <player> - Lists all IPs used by the specified player.");
            sender.sendMessage(ChatColor.DARK_GREEN + "/alts <player> - Lists all users that have shared an IP with the specified player.");
            sender.sendMessage(ChatColor.DARK_GREEN + "/iplogger remove <player/ip> - Removes a player or IP from the database.");
            sender.sendMessage(ChatColor.DARK_GREEN + "/iplogger whitelist <add/remove> <player> - Prevents a players alts from being stored.");
            return true;
        }

        if (args[0].equals("remove")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /iplogger remove <username/ip>");
                return true;
            }

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM users WHERE lower(name)=?");
                pstmt.setString(1, args[1].toLowerCase());
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") > 0) {
                    pstmt = c.prepareStatement("DELETE FROM users WHERE lower(name)=?");
                    pstmt.setString(1, args[1].toLowerCase());
                    pstmt.executeUpdate();
                    pstmt.close();

                    sender.sendMessage(ChatColor.DARK_GREEN + "That username has been removed from the database.");
                    return true;
                }

                pstmt = c.prepareStatement("SELECT count(*) FROM users WHERE ip=?");
                pstmt.setString(1, args[1]);
                rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") > 0) {
                    pstmt = c.prepareStatement("DELETE FROM users WHERE ip=?");
                    pstmt.setString(1, args[1]);
                    pstmt.executeUpdate();
                    pstmt.close();

                    sender.sendMessage(ChatColor.DARK_GREEN + "That IP has been removed from the database.");
                    return true;
                }

                pstmt.close();

                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That username or IP could not be found in the database.");
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/iplogger remove: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        if (args[0].equals("whitelist")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "IPLogger whitelist commands: ");
                sender.sendMessage(ChatColor.DARK_GREEN + "/iplogger whitelist <add/remove> <username>");
                sender.sendMessage(ChatColor.DARK_GREEN + "/iplogger whitelist list");
                return true;
            }

            if (args[1].equals("list")) {
                if (plugin.whitelist.size() < 1) {
                    sender.sendMessage(ChatColor.DARK_GREEN + "No usernames have been added to the whitelist.");
                    return true;
                }

                String output = "";
                boolean first = true;

                for (String name : plugin.whitelist) {
                    if (!first) output += ", ";
                    output += name;
                    first = false;
                }

                sender.sendMessage(ChatColor.DARK_GREEN + "Names added to the IP whitelist:");
                sender.sendMessage(ChatColor.GOLD + output);
                return true;
            }

            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_GREEN + "IPLogger whitelist commands: ");
                sender.sendMessage(ChatColor.DARK_GREEN + "/iplogger whitelist <add/remove> <username>");
                sender.sendMessage(ChatColor.DARK_GREEN + "/iplogger whitelist list");
                return true;
            }


            String username = args[2].toLowerCase();

            if (args[1].equals("add")) {
                if (plugin.whitelist.contains(username)) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That username is already whitelisted.");
                    return true;
                }

                try {
                    Connection c = plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("INSERT INTO whitelist (name) VALUES (?)");
                    pstmt.setString(1, username);
                    pstmt.executeUpdate();
                    pstmt.close();

                    plugin.whitelist.add(username);

                    sender.sendMessage(ChatColor.DARK_GREEN + "That username has been whitelisted.");
                    return true;
                } catch (Exception e) {
                    plugin.getLogger().severe("/iplogger whitelist add: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                    return true;
                }
            }

            if (args[1].equals("remove")) {
                if (!plugin.whitelist.contains(username)) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That username is not whitelisted.");
                    return true;
                }

                try {
                    Connection c = plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("DELETE FROM whitelist WHERE name=?");
                    pstmt.setString(1, username);
                    pstmt.executeUpdate();
                    pstmt.close();

                    plugin.whitelist.remove(username);

                    sender.sendMessage(ChatColor.DARK_GREEN + "That username has been removed from the whitelist.");
                    return true;
                } catch (Exception e) {
                    plugin.getLogger().severe("/iplogger whitelist remove: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                    return true;
                }
            }

            sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /iplogger whitelist <add/remove> <username>");
            return true;
        }

        Bukkit.dispatchCommand(sender, "iplogger help");
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();

        if (args.length == 1 && sender.hasPermission("iplogger.admin")) {
            tabComplete.add("remove");
            tabComplete.add("whitelist");
        }

        if (args.length == 2 && sender.hasPermission("iplogger.admin")) {
            if (args[0].equals("remove")) {
                tabComplete.add("<player>");
                tabComplete.add("<IP>");
            }
            if (args[0].equals("whitelist")) {
                tabComplete.add("remove");
                tabComplete.add("add");
                tabComplete.add("list");
            }
        }

        if (args.length == 3 && sender.hasPermission("iplogger.admin")) {
            if (args[0].equals("whitelist") && !args[1].equals("list")) tabComplete.add("<username>");
        }

        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<String>()) : null;
    }
}
