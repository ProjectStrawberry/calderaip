package com.gmail.jd4656.iplogger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CommandAlts implements CommandExecutor {
    private IPLogger plugin;

    CommandAlts(IPLogger p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("iplogger.admin")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        if (args.length < 1) {
            sender.sendMessage(ChatColor.GOLD + "Usage: /alts <player> - Lists all players that have shared an IP with the specified player.");
            return true;
        }

        Player player = Bukkit.getPlayer(args[0]);
        String uuid;

        if (player != null) {
            uuid = player.getUniqueId().toString();
        } else {
            try {
                PreparedStatement pstmt;
                ResultSet rs;
                Connection c = plugin.getConnection();

                pstmt = c.prepareStatement("SELECT count(*) FROM users WHERE lower(name)=?");
                pstmt.setString(1, args[0].toLowerCase());

                rs = pstmt.executeQuery();
                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                    pstmt.close();
                    return true;
                } else {
                    pstmt = c.prepareStatement("SELECT * FROM users WHERE lower(name)=?");
                    pstmt.setString(1, args[0].toLowerCase());

                    rs = pstmt.executeQuery();
                    uuid = rs.getString("uuid");
                }
            } catch (Exception e) {
                plugin.getLogger().severe("/alts: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        try {
            PreparedStatement pstmt;
            ResultSet rs;
            Connection c = plugin.getConnection();

            pstmt = c.prepareStatement("SELECT * FROM users WHERE uuid=?");
            pstmt.setString(1, uuid);
            rs = pstmt.executeQuery();

            List<String> ips = new ArrayList<>();
            boolean first = true;
            String output = "";
            while (rs.next()) {
                ips.add(rs.getString("ip"));
            }

            for (String ip : ips) {
                pstmt = c.prepareStatement("SELECT * FROM users WHERE ip = ? AND uuid != ?");
                pstmt.setString(1, ip);
                pstmt.setString(2, uuid);

                rs = pstmt.executeQuery();
                while (rs.next()) {
                    if (!first) {
                        output += ", ";
                    } else {
                        first = false;
                    }
                    output += rs.getString("name") + " (" + rs.getString("ip") + ")";
                }
            }

            if (output.length() < 1) {
                sender.sendMessage(ChatColor.GOLD + "No users have shared an IP with " + args[0]);
                return true;
            }

            sender.sendMessage(ChatColor.DARK_GREEN + "Users that have shared an IP with " + ChatColor.GOLD + args[0] + ":");
            sender.sendMessage(ChatColor.GOLD + output);

            pstmt.close();
            return true;
        } catch (Exception e) {
            plugin.getLogger().severe("/alts (2): " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
            return true;
        }
    }
}
